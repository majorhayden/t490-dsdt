/*
 * Intel ACPI Component Architecture
 * AML/ASL+ Disassembler version 20190509 (64-bit version)
 * Copyright (c) 2000 - 2019 Intel Corporation
 * 
 * Disassembly of msdm.dat, Fri Jan 24 20:12:52 2020
 *
 * ACPI Data Table [MSDM]
 *
 * Format: [HexOffset DecimalOffset ByteLength]  FieldName : FieldValue
 */

[000h 0000   4]                    Signature : "MSDM"    [Microsoft Data Management table]
[004h 0004   4]                 Table Length : 00000055
[008h 0008   1]                     Revision : 03
[009h 0009   1]                     Checksum : FA
[00Ah 0010   6]                       Oem ID : "LENOVO"
[010h 0016   8]                 Oem Table ID : "TP-N2R  "
[018h 0024   4]                 Oem Revision : 00001006
[01Ch 0028   4]              Asl Compiler ID : "PTEC"
[020h 0032   4]        Asl Compiler Revision : 00000002

[024h 0036  49] Software Licensing Structure : \
    01 00 00 00 00 00 00 00 01 00 00 00 00 00 00 00 \
    1D 00 00 00 38 4E 33 33 36 2D 50 47 4B 47 33 2D \
    37 4B 37 47 46 2D 50 59 4A 59 52 2D 32 4B 43 4B \
    43 

Raw Table Data: Length 85 (0x55)

    0000: 4D 53 44 4D 55 00 00 00 03 FA 4C 45 4E 4F 56 4F  // MSDMU.....LENOVO
    0010: 54 50 2D 4E 32 52 20 20 06 10 00 00 50 54 45 43  // TP-N2R  ....PTEC
    0020: 02 00 00 00 01 00 00 00 00 00 00 00 01 00 00 00  // ................
    0030: 00 00 00 00 1D 00 00 00 38 4E 33 33 36 2D 50 47  // ........8N336-PG
    0040: 4B 47 33 2D 37 4B 37 47 46 2D 50 59 4A 59 52 2D  // KG3-7K7GF-PYJYR-
    0050: 32 4B 43 4B 43                                   // 2KCKC
