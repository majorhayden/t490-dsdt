/*
 * Intel ACPI Component Architecture
 * AML/ASL+ Disassembler version 20190509 (64-bit version)
 * Copyright (c) 2000 - 2019 Intel Corporation
 * 
 * Disassembly of slic.dat, Fri Jan 24 20:12:52 2020
 *
 * ACPI Data Table [SLIC]
 *
 * Format: [HexOffset DecimalOffset ByteLength]  FieldName : FieldValue
 */

[000h 0000   4]                    Signature : "SLIC"    [Software Licensing Description Table]
[004h 0004   4]                 Table Length : 00000176
[008h 0008   1]                     Revision : 01
[009h 0009   1]                     Checksum : DC
[00Ah 0010   6]                       Oem ID : "LENOVO"
[010h 0016   8]                 Oem Table ID : "TP-N2R  "
[018h 0024   4]                 Oem Revision : 00001006
[01Ch 0028   4]              Asl Compiler ID : "PTEC"
[020h 0032   4]        Asl Compiler Revision : 00000002

[024h 0036 338] Software Licensing Structure : \
    00 00 00 00 9C 00 00 00 06 02 00 00 00 24 00 00 \
    52 53 41 31 00 04 00 00 01 00 01 00 69 16 4A 9F \
    B1 4B 3A FB 80 20 AA AF C4 F9 3E C1 80 49 EE 6A \
    65 26 72 1E CD BF 5F 2F 96 D6 C0 0A 92 F5 06 B5 \
    00 B2 3B 29 02 E2 4C 8D C2 F2 BC 41 77 9C 70 F0 \
    F3 1B 09 D2 63 5A DC A8 83 F8 5E C9 15 95 F9 FA \
    FD DC 05 B7 4D 67 7F 2D B3 84 33 20 E1 D1 79 2A \
    A7 6A 77 D1 B6 20 2A 76 42 C5 D5 E9 B6 43 40 55 \
    44 C3 C9 37 99 5F 41 97 70 F3 D1 F6 07 EC 7B 1A \
    29 A1 C1 F1 91 FD 48 86 6E 3E CE CB 01 00 00 00 \
    B6 00 00 00 00 00 02 00 4C 45 4E 4F 56 4F 54 50 \
    2D 4E 32 52 20 20 57 49 4E 44 4F 57 53 20 01 00 \
    02 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 \
    00 00 42 C9 69 3E 09 3B 27 B2 C1 4E 46 DB 6F E2 \
    1C 29 19 B3 5B F3 3B 2A 0C 90 27 A0 A7 2E 1A 97 \
    72 7A 5B 02 85 CD 52 CD 3E 18 F7 DC E8 F9 1B 22 \
    E0 86 B1 5F 0C D7 33 81 87 DD 82 D6 72 2C 38 08 \
    4F D4 96 D0 91 EE C8 B1 D0 37 A0 49 DF AE 7F ED \
    A5 A3 4E 45 9F 65 EC C6 DF 79 15 C2 3E D5 27 CB \
    A8 57 CB 8E AB 53 23 E2 D5 DF 9F FA 79 B6 C6 0B \
    52 DB E9 1A A6 BB 9F B8 50 57 2E FA 47 74 8A D3 \
    22 60 

Raw Table Data: Length 374 (0x176)

    0000: 53 4C 49 43 76 01 00 00 01 DC 4C 45 4E 4F 56 4F  // SLICv.....LENOVO
    0010: 54 50 2D 4E 32 52 20 20 06 10 00 00 50 54 45 43  // TP-N2R  ....PTEC
    0020: 02 00 00 00 00 00 00 00 9C 00 00 00 06 02 00 00  // ................
    0030: 00 24 00 00 52 53 41 31 00 04 00 00 01 00 01 00  // .$..RSA1........
    0040: 69 16 4A 9F B1 4B 3A FB 80 20 AA AF C4 F9 3E C1  // i.J..K:.. ....>.
    0050: 80 49 EE 6A 65 26 72 1E CD BF 5F 2F 96 D6 C0 0A  // .I.je&r..._/....
    0060: 92 F5 06 B5 00 B2 3B 29 02 E2 4C 8D C2 F2 BC 41  // ......;)..L....A
    0070: 77 9C 70 F0 F3 1B 09 D2 63 5A DC A8 83 F8 5E C9  // w.p.....cZ....^.
    0080: 15 95 F9 FA FD DC 05 B7 4D 67 7F 2D B3 84 33 20  // ........Mg.-..3 
    0090: E1 D1 79 2A A7 6A 77 D1 B6 20 2A 76 42 C5 D5 E9  // ..y*.jw.. *vB...
    00A0: B6 43 40 55 44 C3 C9 37 99 5F 41 97 70 F3 D1 F6  // .C@UD..7._A.p...
    00B0: 07 EC 7B 1A 29 A1 C1 F1 91 FD 48 86 6E 3E CE CB  // ..{.).....H.n>..
    00C0: 01 00 00 00 B6 00 00 00 00 00 02 00 4C 45 4E 4F  // ............LENO
    00D0: 56 4F 54 50 2D 4E 32 52 20 20 57 49 4E 44 4F 57  // VOTP-N2R  WINDOW
    00E0: 53 20 01 00 02 00 00 00 00 00 00 00 00 00 00 00  // S ..............
    00F0: 00 00 00 00 00 00 42 C9 69 3E 09 3B 27 B2 C1 4E  // ......B.i>.;'..N
    0100: 46 DB 6F E2 1C 29 19 B3 5B F3 3B 2A 0C 90 27 A0  // F.o..)..[.;*..'.
    0110: A7 2E 1A 97 72 7A 5B 02 85 CD 52 CD 3E 18 F7 DC  // ....rz[...R.>...
    0120: E8 F9 1B 22 E0 86 B1 5F 0C D7 33 81 87 DD 82 D6  // ..."..._..3.....
    0130: 72 2C 38 08 4F D4 96 D0 91 EE C8 B1 D0 37 A0 49  // r,8.O........7.I
    0140: DF AE 7F ED A5 A3 4E 45 9F 65 EC C6 DF 79 15 C2  // ......NE.e...y..
    0150: 3E D5 27 CB A8 57 CB 8E AB 53 23 E2 D5 DF 9F FA  // >.'..W...S#.....
    0160: 79 B6 C6 0B 52 DB E9 1A A6 BB 9F B8 50 57 2E FA  // y...R.......PW..
    0170: 47 74 8A D3 22 60                                // Gt.."`
