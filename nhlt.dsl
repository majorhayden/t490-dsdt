/*
 * Intel ACPI Component Architecture
 * AML/ASL+ Disassembler version 20190509 (64-bit version)
 * Copyright (c) 2000 - 2019 Intel Corporation
 * 
 * Disassembly of nhlt.dat, Fri Jan 24 20:12:52 2020
 *
 * ACPI Data Table [NHLT]
 *
 * Format: [HexOffset DecimalOffset ByteLength]  FieldName : FieldValue
 */

[000h 0000   4]                    Signature : "NHLT"    
[004h 0004   4]                 Table Length : 0000002D
[008h 0008   1]                     Revision : 00
[009h 0009   1]                     Checksum : 67
[00Ah 0010   6]                       Oem ID : "LENOVO"
[010h 0016   8]                 Oem Table ID : "TP-N2R  "
[018h 0024   4]                 Oem Revision : 00001006
[01Ch 0028   4]              Asl Compiler ID : "PTEC"
[020h 0032   4]        Asl Compiler Revision : 00000002


**** Unknown ACPI table signature [NHLT]


Raw Table Data: Length 45 (0x2D)

    0000: 4E 48 4C 54 2D 00 00 00 00 67 4C 45 4E 4F 56 4F  // NHLT-....gLENOVO
    0010: 54 50 2D 4E 32 52 20 20 06 10 00 00 50 54 45 43  // TP-N2R  ....PTEC
    0020: 02 00 00 00 00 04 00 00 00 DE AD BE EF           // .............
