/*
 * Intel ACPI Component Architecture
 * AML/ASL+ Disassembler version 20190509 (64-bit version)
 * Copyright (c) 2000 - 2019 Intel Corporation
 * 
 * Disassembly of batb.dat, Fri Jan 24 20:12:52 2020
 *
 * ACPI Data Table [BATB]
 *
 * Format: [HexOffset DecimalOffset ByteLength]  FieldName : FieldValue
 */

[000h 0000   4]                    Signature : "BATB"    
[004h 0004   4]                 Table Length : 0000004A
[008h 0008   1]                     Revision : 02
[009h 0009   1]                     Checksum : 8C
[00Ah 0010   6]                       Oem ID : "LENOVO"
[010h 0016   8]                 Oem Table ID : "TP-N2R  "
[018h 0024   4]                 Oem Revision : 00001006
[01Ch 0028   4]              Asl Compiler ID : "PTEC"
[020h 0032   4]        Asl Compiler Revision : 00000002


**** Unknown ACPI table signature [BATB]


Raw Table Data: Length 74 (0x4A)

    0000: 42 41 54 42 4A 00 00 00 02 8C 4C 45 4E 4F 56 4F  // BATBJ.....LENOVO
    0010: 54 50 2D 4E 32 52 20 20 06 10 00 00 50 54 45 43  // TP-N2R  ....PTEC
    0020: 02 00 00 00 E8 63 95 D2 E1 CF 41 4D 8E 54 DA 43  // .....c....AM.T.C
    0030: 22 FE DE 5C 36 00 00 A0 F1 8A 00 00 00 00 00 00  // "..\6...........
    0040: F0 8A 00 00 00 00 00 00 01 00                    // ..........
